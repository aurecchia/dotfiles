#!/usr/bin/env bash

cd "$(dirname "${BASH_SOURCE}")"

function sync() {
        rsync --exclude ".git/" --exclude "link.sh" --exclude "README.md" \
            -av --no-perms . ~
        source ~/.bashrc
}

if [ "$1" == "--force" -o "$1" == "-f" ]; then
        sync
else
        read -p "This may overwrite existing files in your home directory. Are you sure? (y/n) " -n 1
        echo
        if [[ $REPLY =~ ^[Yy]$ ]]; then
                sync
        fi
fi
unset sync

