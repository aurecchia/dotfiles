alias toc='cd ~/Dropbox/USI/5th\ Semester/TOC'
alias ta='cd ~/Dropbox/USI/5th\ Semester/TA'
alias cg='cd ~/Dropbox/USI/5th\ Semester/CG'
alias EE='cd ~/Dropbox/USI/5th\ Semester/EE'
alias IR='cd ~/Dropbox/USI/5th\ Semester/IR'

alias ll='ls -alhF'
alias la='ls -ACF'
alias l='ls -CF'

alias e='gvim'
compilemd () { pandoc --template=/home/auri/.pandoc/default.latex --listing "$@".md -o "$@".pdf; }
alias mdc=compilemd

alias here='caja ./ 2> /dev/null &'

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

